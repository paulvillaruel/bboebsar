# README #

### What is this repository for? ###

* EBS AR interface for DPWorld Berbera

### Opening the solution ###

* Go to *Downloads* -> *Download Repository* to get offline copy of the source code
* Or git clone repository if you want to track the versioned copy
* Customization code can be found in [ARExport.vb](https://bitbucket.org/paulvillaruel/bboebsar/src/02c7e12306c72282ba8263de251484179d7c9f9f/EBS.BBO.ARInterface/ARExport.vb?at=master&fileviewer=file-view-default)  


### AR Interface Library Details ###

* Library: _EBS.BBO.ARInterface_
* Namespace: _Interfaces_
* Version Class Name: _ARExport_
* .NET Framework: _3.5_ using Visual Studio 2015 or later


### Deployment guidelines ###

* Place assembly in EBS appserver directory: *E:\WEBAPPS\wwwroot\VirtualSites\EBS\main\bin*
* Open EBS Table Maintenance module, open Revenue AR Mapping.
* Configure settings as per above library details 

### Contact Person ###

* EBS AR Mapping to Oracle Fusion - Paul Villaruel paul.villaruel@dpworld.com 
* EBS Core/API - Paul Yip paul.yip@dpworld.com