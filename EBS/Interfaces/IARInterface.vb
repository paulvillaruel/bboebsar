﻿Namespace Interfaces
    Public Interface IARInterface
        Structure Tables
            Shared ReadOnly SuitableInvoice As String = "SuitableInvoice"
            Shared ReadOnly OutstandingInvoice As String = "OutstandingInvoice"
            Shared ReadOnly SuitableSettlement As String = "SuitableSettlement"
            Shared ReadOnly OutstandingSettlement As String = "OutstandingSettlement"

            Shared ReadOnly SuitableContainer As String = "SuitableContainer"
            Shared ReadOnly OutstandingContainer As String = "OutstandingContainer"
        End Structure
        Sub PreProcess(ByVal filePathPrefix As String, ByVal archivePathPrefix As String)
        Sub ExportInvoice(ByVal connection As IDbConnection, ByVal transaction As IDbTransaction, ByVal filePathPrefix As String, ByVal archivePathPrefix As String, ByVal dataToExport As DataSet)
        Sub PostProcessOnSucceed(ByVal filePathPrefix As String, ByVal archivePathPrefix As String)
        Sub PostProcessOnError(ByVal filePathPrefix As String, ByVal archivePathPrefix As String, ByVal ex As Exception)
    End Interface
End Namespace
