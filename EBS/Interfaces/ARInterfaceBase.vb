﻿Imports System.IO
'Imports EBS.Utility
'Imports EBS.Exceptions

Namespace Interfaces
    Public MustInherit Class ARInterfaceBase
        Implements IARInterface
        Private _ExportFileName As String
        Protected Overridable Property ExportFileName As String
            Get
                Return _ExportFileName
            End Get
            Set(ByVal value As String)
                _ExportFileName = value & ".EBS"
            End Set
        End Property

        Protected ReadOnly Property TempExportFileName As String
            Get
                Return ExportFileName & ".tmp"
            End Get
        End Property

        Private _OutstandingOnly As Boolean
        Protected Property OutstandingOnly As Boolean
            Get
                Return _OutstandingOnly
            End Get
            Set(ByVal value As Boolean)
                _OutstandingOnly = value
            End Set
        End Property

        Sub New(ByVal fileName As String)
            ExportFileName = fileName
        End Sub

        Sub New(ByVal fileName As String, ByVal outstandingOnly As Boolean)
            ExportFileName = fileName
            Me.OutstandingOnly = outstandingOnly
        End Sub

        Protected Overridable Sub PreProcess(ByVal filePathPrefix As String, ByVal archivePathPrefix As String) Implements IARInterface.PreProcess
            'validation
            'If ExportFileName Is Nothing OrElse ExportFileName.Trim = "" Then
            '    Throw New ARInterfaceException(ARInterfaceException.ARInterfaceError.NoFileNameInput)
            'End If
            'If InStr(ExportFileName, ":") <> 0 OrElse InStr(ExportFileName, "\\") <> 0 OrElse
            '    InStr(ExportFileName, "/") <> 0 OrElse InStr(ExportFileName, "\") <> 0 Then
            '    Throw New ARInterfaceException(ARInterfaceException.ARInterfaceError.InvalidFileName)
            'End If
            'If filePathPrefix Is Nothing OrElse filePathPrefix.Trim = "" Then
            '    Throw New ARInterfaceException(ARInterfaceException.ARInterfaceError.NoFilePathDefined)
            'End If
            'If File.Exists(EBS.Utility.FileHelper.ConvertToFullPath(ExportFileName, filePathPrefix)) Then
            '    Throw New ARInterfaceException(ARInterfaceException.ARInterfaceError.FileNameExists)
            'End If
            'If File.Exists(EBS.Utility.FileHelper.ConvertToFullPath(TempExportFileName, filePathPrefix)) Then
            '    Throw New ARInterfaceException(ARInterfaceException.ARInterfaceError.TempFileNameExists)
            'End If
            'If archivePathPrefix Is Nothing OrElse archivePathPrefix.Trim = "" Then
            '    Throw New ARInterfaceException(ARInterfaceException.ARInterfaceError.NoArchivePathDefined)
            'End If
            'If File.Exists(EBS.Utility.FileHelper.ConvertToFullPath(ExportFileName, archivePathPrefix)) Then
            '    Throw New ARInterfaceException(ARInterfaceException.ARInterfaceError.FileNameExistsInArchive)
            'End If
        End Sub

        Protected MustOverride Sub ExportInvoice(ByVal connection As IDbConnection, ByVal transaction As IDbTransaction, ByVal filePathPrefix As String, ByVal archivePathPrefix As String, ByVal dataToExport As DataSet) Implements IARInterface.ExportInvoice

        Protected Overridable Sub PostProcessOnSucceed(ByVal filePathPrefix As String, ByVal archivePathPrefix As String) Implements IARInterface.PostProcessOnSucceed
            'Dim filePath As String = EBS.Utility.FileHelper.ConvertToFullPath(ExportFileName, filePathPrefix)
            'Dim tempFilePath As String = EBS.Utility.FileHelper.ConvertToFullPath(TempExportFileName, filePathPrefix)
            'File.Move(tempFilePath, filePath)
        End Sub

        Protected Overridable Sub PostProcessOnError(ByVal filePathPrefix As String, ByVal archivePathPrefix As String, ByVal ex As Exception) Implements IARInterface.PostProcessOnError
            'Dim filePath As String = EBS.Utility.FileHelper.ConvertToFullPath(ExportFileName, filePathPrefix)
            'If File.Exists(filePath) Then
            '    File.Delete(filePath)
            'End If
            'Dim tempFilePath As String = EBS.Utility.FileHelper.ConvertToFullPath(TempExportFileName, filePathPrefix)
            'If File.Exists(tempFilePath) Then
            '    File.Delete(tempFilePath)
            'End If
        End Sub
    End Class
End Namespace
