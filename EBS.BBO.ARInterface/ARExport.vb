﻿Imports System.IO
Imports EBS.Interfaces
Imports System.Text

Namespace Interfaces

    Public Class ARExport
        Inherits EBS.Interfaces.ARInterfaceBase

        Sub New(ByVal fileName As String)
            MyBase.New(fileName)
        End Sub

        Sub New(ByVal fileName As String, ByVal outstandingOnly As Boolean)
            MyBase.New(fileName, outstandingOnly)
        End Sub

        Protected Overrides Sub ExportInvoice(ByVal connection As IDbConnection, ByVal transaction As IDbTransaction, ByVal filePathPrefix As String, ByVal archivePathPrefix As String, ByVal dataToExport As DataSet)
            '---------------- Preparation [Start] ----------------------
            Dim dtARInvoice As DataTable = Nothing
            Dim dtARSettlement As DataTable = Nothing

            If OutstandingOnly Then
                If dataToExport.Tables.Contains(IARInterface.Tables.OutstandingInvoice) Then
                    dtARInvoice = dataToExport.Tables(IARInterface.Tables.OutstandingInvoice)
                End If
                If dataToExport.Tables.Contains(IARInterface.Tables.OutstandingSettlement) Then
                    dtARSettlement = dataToExport.Tables(IARInterface.Tables.OutstandingSettlement)
                End If
            Else
                If dataToExport.Tables.Contains(IARInterface.Tables.SuitableInvoice) Then
                    dtARInvoice = dataToExport.Tables(IARInterface.Tables.SuitableInvoice)
                End If
                If dataToExport.Tables.Contains(IARInterface.Tables.SuitableSettlement) Then
                    dtARSettlement = dataToExport.Tables(IARInterface.Tables.SuitableSettlement)
                End If
            End If
            '---------------- Preparation [End] ----------------------




            '---------------- Generate files ----------------------
            '-- dtARInvoice         :   data table maintaining invoice information
            '-- dtARSettlement      :   data table maintaining receipt information
            '--
            '--     Local customization
            '--         Render the output, and generate the files
            '------------------------------------------------------

            '------------- Generate invoice + receipts CSV----------
            '              for USD and SHL                
            '              Paul Villaruel 2018-08-27
            '
            '   1. Filter dtArInvoice and dtArSettlement "USD and SHL"
            '   2. Form the output dtOut for Invoice and Receipts
            '   3. Loop through the filtered dtArInvoice and dtArSettlement
            '   4. Form drOut dataRow (based on mapping) for Invoice and Receipts
            '   5. Add drOut to dtOut for Invoice and Receipts
            '   6. Pass dtOut to csvWriter for Invoice and Receipts
            '------------------------------------------------------

            Dim currency As String = ""
            Dim dtARInvoiceOut As New DataTable
            Dim dtARReceiptOut As New DataTable

            '
            ' USD 
            '

            currency = "USD"

            Dim drARInvoice As DataRow() = dtARInvoice.Select("BILL_CURRENCY_CODE = '" & currency & "'")
            dtARInvoiceOut = formARInvoiceOutDataTable()

            For Each rowInvoice In drARInvoice
                Dim drARInvoiceOut As DataRow = dtARInvoiceOut.NewRow
                With drARInvoiceOut
                    .Item("Business Unit Name") = My.Settings.businessUnitId
                    .Item("Transaction Type Name") = rowInvoice.Item("<Transaction.TypeName>")
                    .Item("Payment Terms") = rowInvoice.Item("PAYMENT_METHOD")
                    .Item("Transaction Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Accounting Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Bill-to Customer Account Number") = rowInvoice.Item("CUST_AR_ACCT_NUMBER")
                    .Item("Original System Bill-to Customer Reference") = rowInvoice.Item("<Customer.Name>")
                    .Item("Bill-to Customer Site Number") = rowInvoice.Item("CUST_AR_ACCT_NUMBER")
                    .Item("Original System Bill-to Customer Address Reference") = rowInvoice.Item("BATCH_SOURCE_NAME")
                    .Item("Transaction Line Description") = rowInvoice.Item("DESCRIPTION")
                    .Item("Currency Code") = rowInvoice.Item("BILL_CURRENCY_CODE")
                    .Item("Currency Conversion Type") = ""
                    .Item("Currency Conversion Date") = ""
                    .Item("Conversion Rate") = ""
                    .Item("Transaction Line Quantity") = rowInvoice.Item("QUANTITY1")
                    .Item("Unit of Measure Code") = rowInvoice.Item("UOM_NAME1")
                    .Item("Unit Selling Price") = rowInvoice.Item("UNIT_RATE")
                    .Item("Transaction Line Amount") = rowInvoice.Item("LINE_AMOUNT")
                    .Item("Revenue Scheduling Rule Name") = rowInvoice.Item("INVOICE_TYPE_DESC")
                    .Item("Revenue Scheduling Rule Start Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Revenue Scheduling Rule End Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Tax Classification Code") = rowInvoice.Item("TAX_CODE")
                    .Item("*Memo Line Name") = rowInvoice.Item("DESCRIPTION")
                    .Item("Revenue Account Code Combination") = ""
                    .Item("Receivables Account Code Combination") = ""
                    .Item("Unit Standard Price") = rowInvoice.Item("UNIT_RATE")
                    .Item("Transaction Line Translated Description") = ""
                    .item("Line Transactions Flexfield Context") = rowInvoice.item("Billing Details")
                    .Item("Line Transactions Flexfield Segment 1") = rowInvoice.Item("<Customer.Name>")
                    .Item("Line Transactions Flexfield Segment 2") = rowInvoice.Item("<Customer.Type>")
                    .Item("Line Transactions Flexfield Segment 3") = rowInvoice.Item("VESSEL_NAME")
                    .Item("Line Transactions Flexfield Segment 4") = rowInvoice.Item("<TDR.DocRegNo> or <CFS.RequestNo>")
                    .Item("Line Transactions Flexfield Segment 5") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Line Transactions Flexfield Segment 6") = rowInvoice.Item("<Vessel.ArrivalAnchorage>")
                    .Item("Line Transactions Flexfield Segment 7") = IIf(IsDBNull(rowInvoice.Item("VESSEL_DEPARTURE_DATE")), "", Format(rowInvoice.Item("VESSEL_DEPARTURE_DATE"), My.Settings.dateTimeFormat))
                    .item("Line Transactions Flexfield Segment 8") = rowInvoice.item("")
                    .Item("Line Transactions Flexfield Segment 9") = rowInvoice.Item("INVOICE_TYPE_DESC")
                    .Item("Line Transactions Flexfield Segment 10") = rowInvoice.Item("QUANTITY2")
                    .Item("Line Transactions Flexfield Segment 11") = rowInvoice.Item("TRANSACTION_NO")
                    .Item("Line Transactions Flexfield Segment 12") = rowInvoice.Item("VESSEL_OPERATOR_CODE")
                    .Item("Line Transactions Flexfield Segment 13") = rowInvoice.Item("<Vessel.LOA>")
                    .Item("Line Transactions Flexfield Segment 14") = rowInvoice.Item("GRT")
                    .Item("Line Transactions Flexfield Segment 15") = ""
                    .Item("Sales Order Number") = rowInvoice.Item("TRANSACTION_NO")
                    .Item("Sales Order Line Number") = rowInvoice.Item("CHARGE_LINE_NO")
                    .Item("Sales Order Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                End With
                dtARInvoiceOut.Rows.Add(drARInvoiceOut)
            Next


            Dim drARSettlement As DataRow() = dtARSettlement.Select("CURRENCY_CODE = '" & currency & "'")
            dtARReceiptOut = formARReceiptOutDataTable()

            For Each rowARSettlement As DataRow In drARSettlement
                Dim drARReceiptOut As DataRow = dtARInvoiceOut.NewRow
                With drARReceiptOut
                    .Item("Business Unit") = My.Settings.businessUnitId
                    .Item("Receipt Method ID") = getReceiptMethodId(rowARSettlement.Item("BANK_CODE"))
                    .Item("Receipt Method") = rowARSettlement.Item("PAY_METHOD")
                    .Item("Remittance Bank") = rowARSettlement.Item("BANK_CODE")
                    .Item("Remittance Bank Account") = rowARSettlement.Item("BANK_ACCOUNT_NUM")
                    .Item("Receipt Number") = rowARSettlement.Item("RECEIPT_NUMBER")
                    .Item("Receipt Amount") = rowARSettlement.Item("LINE_AMOUNT")
                    .Item("Receipt Date") = IIf(IsDBNull(rowARSettlement.Item("PAY_DATE")), "", Format(rowARSettlement.Item("PAY_DATE"), My.Settings.dateTimeFormat))
                    .Item("Accounting Date") = IIf(IsDBNull(rowARSettlement.Item("PAY_DATE")), "", Format(rowARSettlement.Item("PAY_DATE"), My.Settings.dateTimeFormat))
                    .Item("Conversion Date") = ""
                    .Item("Currency") = currency
                    .Item("Conversion Rate Type") = ""
                    .Item("Conversion Rate") = ""
                    .Item("Document Number") = rowARSettlement.Item("REF_NUMBER")
                    .Item("Customer Name") = rowARSettlement.Item("<Customer.FullName>")
                    .Item("Customer Account Number") = rowARSettlement.Item("<Customer.AccountId>")
                    .Item("Customer Site") = rowARSettlement.Item("<Customer.SiteId>")
                    .Item("Reference Details") = rowARSettlement.Item("INVOICE_ID")
                End With
                dtARReceiptOut.Rows.Add(drARReceiptOut)
            Next

            filePathPrefix = filePathPrefix & "_" & currency
            csvWriter(filePathPrefix & "_INVOICE.csv", dtARInvoiceOut)
            csvWriter(filePathPrefix & "_RECEIPT.csv", dtARReceiptOut)


            '
            ' SHL 
            '

            dtARInvoiceOut.Rows.Clear()
            dtARReceiptOut.Rows.Clear()

            currency = "SHL"

            drARInvoice = dtARInvoice.Select("BILL_CURRENCY_CODE = '" & currency & "'")

            For Each rowInvoice In drARInvoice
                Dim drARInvoiceOut As DataRow = dtARInvoiceOut.NewRow
                With drARInvoiceOut
                    .Item("Business Unit Name") = My.Settings.businessUnitId
                    .Item("Transaction Type Name") = rowInvoice.Item("<Transaction.TypeName>")
                    .Item("Payment Terms") = rowInvoice.Item("PAYMENT_METHOD")
                    .Item("Transaction Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Accounting Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Bill-to Customer Account Number") = rowInvoice.Item("CUST_AR_ACCT_NUMBER")
                    .Item("Original System Bill-to Customer Reference") = rowInvoice.Item("<Customer.Name>")
                    .Item("Bill-to Customer Site Number") = rowInvoice.Item("CUST_AR_ACCT_NUMBER")
                    .Item("Original System Bill-to Customer Address Reference") = rowInvoice.Item("BATCH_SOURCE_NAME")
                    .Item("Transaction Line Description") = rowInvoice.Item("DESCRIPTION")
                    .Item("Currency Code") = rowInvoice.Item("BILL_CURRENCY_CODE")
                    .Item("Currency Conversion Type") = "Corporate"
                    .Item("Currency Conversion Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Conversion Rate") = rowInvoice.Item("EXCHANGE_RATE")
                    .Item("Transaction Line Quantity") = rowInvoice.Item("QUANTITY1")
                    .Item("Unit of Measure Code") = rowInvoice.Item("UOM_NAME1")
                    .Item("Unit Selling Price") = rowInvoice.Item("UNIT_RATE")
                    .Item("Transaction Line Amount") = rowInvoice.Item("LINE_AMOUNT")
                    .Item("Revenue Scheduling Rule Name") = rowInvoice.Item("INVOICE_TYPE_DESC")
                    .Item("Revenue Scheduling Rule Start Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Revenue Scheduling Rule End Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Tax Classification Code") = rowInvoice.Item("TAX_CODE")
                    .Item("Memo Line Name") = rowInvoice.Item("DESCRIPTION")
                    .Item("Revenue Account Code Combination") = ""
                    .Item("Receivables Account Code Combination") = ""
                    .Item("Unit Standard Price") = rowInvoice.Item("UNIT_RATE")
                    .Item("Transaction Line Translated Description") = ""
                    .Item("Line Transactions Flexfield Context") = rowInvoice.Item("Billing Details")
                    .Item("Line Transactions Flexfield Segment 1") = rowInvoice.Item("<Customer.Name>")
                    .Item("Line Transactions Flexfield Segment 2") = rowInvoice.Item("<Customer.Type>")
                    .Item("Line Transactions Flexfield Segment 3") = rowInvoice.Item("VESSEL_NAME")
                    .Item("Line Transactions Flexfield Segment 4") = rowInvoice.Item("<TDR.DocRegNo> or <CFS.RequestNo>")
                    .Item("Line Transactions Flexfield Segment 5") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                    .Item("Line Transactions Flexfield Segment 6") = rowInvoice.Item("<Vessel.ArrivalAnchorage>")
                    .Item("Line Transactions Flexfield Segment 7") = IIf(IsDBNull(rowInvoice.Item("VESSEL_DEPARTURE_DATE")), "", Format(rowInvoice.Item("VESSEL_DEPARTURE_DATE"), My.Settings.dateTimeFormat))
                    .Item("Line Transactions Flexfield Segment 8") = rowInvoice.Item("")
                    .Item("Line Transactions Flexfield Segment 9") = rowInvoice.Item("INVOICE_TYPE_DESC")
                    .Item("Line Transactions Flexfield Segment 10") = rowInvoice.Item("QUANTITY2")
                    .Item("Line Transactions Flexfield Segment 11") = rowInvoice.Item("TRANSACTION_NO")
                    .Item("Line Transactions Flexfield Segment 12") = rowInvoice.Item("VESSEL_OPERATOR_CODE")
                    .Item("Line Transactions Flexfield Segment 13") = rowInvoice.Item("<Vessel.LOA>")
                    .Item("Line Transactions Flexfield Segment 14") = rowInvoice.Item("GRT")
                    .Item("Line Transactions Flexfield Segment 15") = ""
                    .Item("Sales Order Number") = rowInvoice.Item("TRANSACTION_NO")
                    .Item("Sales Order Line Number") = rowInvoice.Item("CHARGE_LINE_NO")
                    .Item("Sales Order Date") = IIf(IsDBNull(rowInvoice.Item("INV_DATE")), "", Format(rowInvoice.Item("INV_VATE"), My.Settings.dateTimeFormat))
                End With
                dtARInvoiceOut.Rows.Add(drARInvoiceOut)
            Next

            drARSettlement = dtARSettlement.Select("CURRENCY_CODE = '" & currency & "'")

            For Each rowARSettlement As DataRow In drARSettlement
                Dim drARReceiptOut As DataRow = dtARInvoiceOut.NewRow
                With drARReceiptOut
                    .Item("Business Unit") = My.Settings.businessUnitId
                    .Item("Receipt Method ID") = getReceiptMethodId(rowARSettlement.Item("BANK_CODE"))
                    .Item("Receipt Method") = rowARSettlement.Item("PAY_METHOD")
                    .Item("Remittance Bank") = rowARSettlement.Item("BANK_CODE")
                    .Item("Remittance Bank Account") = rowARSettlement.Item("BANK_ACCOUNT_NUM")
                    .Item("Receipt Number") = rowARSettlement.Item("RECEIPT_NUMBER")
                    .Item("Receipt Amount") = rowARSettlement.Item("LINE_AMOUNT")
                    .Item("Receipt Date") = IIf(IsDBNull(rowARSettlement.Item("PAY_DATE")), "", Format(rowARSettlement.Item("PAY_DATE"), My.Settings.dateTimeFormat))
                    .Item("Accounting Date") = IIf(IsDBNull(rowARSettlement.Item("PAY_DATE")), "", Format(rowARSettlement.Item("PAY_DATE"), My.Settings.dateTimeFormat))
                    .Item("Conversion Date") = ""
                    .Item("Currency") = currency
                    .Item("Conversion Rate Type") = ""
                    .Item("Conversion Rate") = ""
                    .Item("Document Number") = rowARSettlement.Item("REF_NUMBER")
                    .Item("Customer Name") = rowARSettlement.Item("<Customer.FullName>")
                    .Item("Customer Account Number") = rowARSettlement.Item("<Customer.AccountId>")
                    .Item("Customer Site") = rowARSettlement.Item("<Customer.SiteId>")
                    .Item("Reference Details") = rowARSettlement.Item("INVOICE_ID")
                End With
                dtARReceiptOut.Rows.Add(drARReceiptOut)
            Next

            filePathPrefix = filePathPrefix & "_" & currency
            csvWriter(filePathPrefix & "_INVOICE.csv", dtARInvoiceOut)
            csvWriter(filePathPrefix & "_RECEIPT.csv", dtARReceiptOut)



        End Sub


        Private Function getReceiptMethodId(bankCode As String) As String
            Dim methodId As String = ""
            Select Case bankCode
                Case "Dahabshil" : methodId = My.Settings.receiptMethodIdDahabshil.ToString 'Return "300000001806074"
                Case "Salaam Bank" : methodId = My.Settings.receiptMethodIdSalaam.ToString 'Return "300000001806071"
            End Select
            Return methodId
        End Function

        Private Function formARInvoiceOutDataTable() As DataTable

            Dim tableResult As New DataTable("dtARInvoice")

            Try

                tableResult.Columns.Add("Business Unit Name", GetType(String))
                tableResult.Columns.Add("Transaction Type Name", GetType(String))
                tableResult.Columns.Add("Payment Terms", GetType(String))
                tableResult.Columns.Add("Transaction Date", GetType(String))
                tableResult.Columns.Add("Accounting Date", GetType(String))
                tableResult.Columns.Add("Bill-to Customer Account Number", GetType(String))
                tableResult.Columns.Add("Original System Bill-to Customer Reference", GetType(String))
                tableResult.Columns.Add("Bill-to Customer Site Number", GetType(String))
                tableResult.Columns.Add("Original System Bill-to Customer Address Reference", GetType(String))
                tableResult.Columns.Add("Transaction Line Description", GetType(String))
                tableResult.Columns.Add("Currency Code", GetType(String))
                tableResult.Columns.Add("Currency Conversion Type", GetType(String))
                tableResult.Columns.Add("Currency Conversion Date", GetType(String))
                tableResult.Columns.Add("Conversion Rate", GetType(String))
                tableResult.Columns.Add("Transaction Line Quantity", GetType(String))
                tableResult.Columns.Add("Unit of Measure Code", GetType(String))
                tableResult.Columns.Add("Unit Selling Price", GetType(String))
                tableResult.Columns.Add("Transaction Line Amount", GetType(String))
                tableResult.Columns.Add("Revenue Scheduling Rule Name", GetType(String))
                tableResult.Columns.Add("Revenue Scheduling Rule Start Date", GetType(String))
                tableResult.Columns.Add("Revenue Scheduling Rule End Date", GetType(String))
                tableResult.Columns.Add("Tax Classification Code", GetType(String))
                tableResult.Columns.Add("Memo Line Name", GetType(String))
                tableResult.Columns.Add("Revenue Account Code Combination", GetType(String))
                tableResult.Columns.Add("Receivables Account Code Combination", GetType(String))
                tableResult.Columns.Add("Unit Standard Price", GetType(String))
                tableResult.Columns.Add("Transaction Line Translated Description", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Context", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 1", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 2", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 3", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 4", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 5", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 6", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 7", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 8", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 9", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 10", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 11", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 12", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 13", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 14", GetType(String))
                tableResult.Columns.Add("Line Transactions Flexfield Segment 15", GetType(String))
                tableResult.Columns.Add("Sales Order Number", GetType(String))
                tableResult.Columns.Add("Sales Order Line Number", GetType(String))
                tableResult.Columns.Add("Sales Order Date", GetType(String))

                Return tableResult

            Catch ex As Exception
                Return returnExceptionTable(ex.Message)
            End Try


        End Function

        Private Function formARReceiptOutDataTable() As DataTable

            Dim tableResult As New DataTable("dtARSettlement")

            Try

                tableResult.Columns.Add("Business Unit", GetType(String))
                tableResult.Columns.Add("Receipt Method ID", GetType(String))
                tableResult.Columns.Add("Receipt Method", GetType(String))
                tableResult.Columns.Add("Remittance Bank", GetType(String))
                tableResult.Columns.Add("Remittance Bank Account", GetType(String))
                tableResult.Columns.Add("Receipt Number", GetType(String))
                tableResult.Columns.Add("Receipt Amount", GetType(String))
                tableResult.Columns.Add("Receipt Date", GetType(String))
                tableResult.Columns.Add("Accounting Date", GetType(String))
                tableResult.Columns.Add("Conversion Date", GetType(String))
                tableResult.Columns.Add("Currency", GetType(String))
                tableResult.Columns.Add("Conversion Rate Type", GetType(String))
                tableResult.Columns.Add("Conversion Rate", GetType(String))
                tableResult.Columns.Add("Document Number", GetType(String))
                tableResult.Columns.Add("Customer Name", GetType(String))
                tableResult.Columns.Add("Customer Account Number", GetType(String))
                tableResult.Columns.Add("Customer Site", GetType(String))
                tableResult.Columns.Add("Reference Details", GetType(String))

                Return tableResult

            Catch ex As Exception
                Return returnExceptionTable(ex.Message)
            End Try

        End Function

        Private Function returnExceptionTable(message As String) As DataTable
            Dim tableException As New DataTable("tableException")
            tableException.Columns.Add("Result", GetType(String))
            tableException.Rows.Add(message)
            Return tableException
        End Function




        '
        ' CSVWriter
        '

        Protected Sub csvWriter(ByVal sFileName As String, ByRef dTable As DataTable)
            Dim writer As StreamWriter
            If System.IO.File.Exists(sFileName) = True Then
                System.IO.File.Delete(sFileName)
            End If
            writer = File.CreateText(sFileName)

            '--------Columns Name--------------------------------------------------------------------------- 
            Dim intClmn As Integer = dTable.Columns.Count

            Dim i As Integer = 0
            For i = 0 To intClmn - 1 Step i + 1
                writer.Write("""" + dTable.Columns(i).ColumnName.ToString() + """")
                If i = intClmn - 1 Then
                    writer.Write(" ")
                Else
                    writer.Write(",")
                End If
            Next
            writer.Write(vbNewLine)

            '--------Data By  Columns--------------------------------------------------------------------------- 
            Dim row As DataRow
            For Each row In dTable.Rows

                Dim ir As Integer = 0
                For ir = 0 To intClmn - 1 Step ir + 1
                    writer.Write("""" + row(ir).ToString().Replace("""", """""") + """")
                    If ir = intClmn - 1 Then
                        writer.Write(" ")
                    Else
                        writer.Write(",")
                    End If

                Next
                writer.Write(vbNewLine)
            Next


            If writer IsNot Nothing Then
                writer.Close()
            End If
        End Sub
    End Class

End Namespace
